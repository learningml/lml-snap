SyntaxElementMorph.prototype.labelParts = {
    ...SyntaxElementMorph.prototype.labelParts,
    '%tensortype': {
        type: 'input',
        tags: 'read-only static',
        menu: {
            float32: 'float32',
            int32: 'int32',
            bool: 'bool',
            complex64: 'complex64',
            string: 'string'
        }
    },
    '%tensorarithmetic': {
        type: 'input',
        tags: 'read-only static',
        menu: {
            add: 'add',
            sub: 'sub',
            mul: 'mul',
            div: 'div',
            divNoNan: 'divNoNan',
            floorDiv: 'floorDiv',
            maximun: 'maximun',
            minimun: 'minimun',
            mod: 'mod',
            pow: 'pow',
            squaredDifference: 'squaredDifference',

        }
    },
    '%tensorbasicmath': {
        type: 'input',
        tags: 'read-only static',
        menu: {
            abs: 'abs',
            acos: 'acos',
            acosh: 'acosh',
            asin: 'asin',
            asinh: 'asinh',
            atan: 'atan',
            atan2: 'atan2',
            atanh: 'atanh',
            ceil: 'ceil',
            clipByValue: 'clipByValue',
            cos: 'cos',
            cosh: 'cosh',
            elu: 'elu',
            erf: 'erf',
            exp: 'exp',
            expm1: 'expm1',
            floor: 'floor',
            isFinite: 'isFinite',
            isInf: 'isInf',
            isNaN: 'isNaN',
            leakyRelu: 'leakyRelu',
            log: 'log',
            log1p: 'log1p',
            logSigmoid: 'logSigmoid',
            neg: 'neg',
            prelu: 'prelu',
            reciprocal: 'reciprocal',
            relu: 'relu',
            relu6: 'relu6',
            round: 'round',
            rsqrt: 'rsqrt',
            selu: 'selu',
            sigmoid: 'sigmoid',
            sign: 'sign',
            sin: 'sin',
            sinh: 'sinh',
            softplus: 'softplus',
            sqrt: 'sqrt',
            square: 'square',
            step: 'step',
            tan: 'tan',
            tanh: 'tanh'
        }
    },
    '%tensormatrices': {
        type: 'input',
        tags: 'read-only static',
        menu: {
            dot: 'dot',
            euclideanNorm: 'euclideanNorm',
            norm: 'norm',
            outerProduct: 'outerProduct',
            transpose: 'transpose'
        }
    },

    '%tensorlists': {
        type: 'multi',
        slots: '%l',
        defaults: 1
    },

    '%tensoractivations': {
        type: 'input',
        tags: 'read-only static',
        menu: {
            none: null,
            elu: 'elu',
            hardSigmoid: 'hardSigmoid',
            linear: 'linear',
            relu: 'relu',
            relu6: 'relu6',
            selu: 'selu',
            sigmoid: 'sigmoid',
            softmax: 'softmax',
            softplus: 'softplus',
            softsign: 'softsign',
            tanh: 'tanh',
            swish: 'swish',
            mish: 'mish'
        }
    },

    '%tensorlayers': {
        type: 'multi',
        slots: '%s',
        label: 'with layers',
        tags: 'widget',
        defaults: 1
    },

    '%tensoroptimizers': {
        type: 'input',
        tags: 'read-only static',
        menu: {
            sgd: 'sgd',
            momentum: 'momentum',
            adagrad: 'adagrad',
            adadelta: 'adadelta',
            adam: 'adam',
            adamax: 'adamax',
            rmsprop: 'rmsprop'
        }
    },
    '%tensorloss': {
        type: 'input',
        tags: 'read-only static',
        menu: {
            categoricalCrossentropy: 'categoricalCrossentropy',
            absoluteDifference: 'absoluteDifference',
            computeWeightedLoss: 'computeWeightedLoss',
            cosineDistance: 'cosineDistance',
            hingeLoss: 'hingeLoss',
            huberLoss: 'huberLoss',
            logLoss: 'logLoss',
            meanSquaredError: 'meanSquaredError',
            sigmoidCrossEntropy: 'sigmoidCrossEntropy',
            softmaxCrossEntropy: 'softmaxCrossEntropy'
        }
    },
    '%tensormetric': {
        type: 'input',
        tags: 'read-only static',
        menu: {
            accuracy: 'accuracy',
            binaryAccuracy: 'binaryAccuracy',
            binaryCrossentropy: 'binaryCrossentropy',
            categoricalAccuracy: 'categoricalAccuracy',
            categoricalCrossentropy: 'categoricalCrossentropy',
            cosineProximity: 'cosineProximity',
            meanAbsoluteError: 'meanAbsoluteError',
            meanAbsolutePercentageError: 'meanAbsolutePercentageError',
            precision : 'precision ',
            recall: 'recall',
            sparseCategoricalAccuracy: 'sparseCategoricalAccuracy ',

        }
    },
    '%tensorrandom': {
        type: 'input',
        tags: 'read-only static',
        menu: {
            randomNormal: 'randomNormal',
            randomStandardNormal: 'randomStandardNormal',
            randomUniform: 'randomUniform',
        }
    },
    

}

