// Add `Tensorflow` category
SpriteMorph.prototype.categories.push('tensorflow');
SpriteMorph.prototype.blockColor['tensorflow'] = new Color(100, 161, 120);

// Block decorator for Tensorflow

SpriteMorph.prototype.originalInitBlocks = SpriteMorph.prototype.initBlocks;
SpriteMorph.prototype.initTensorflowBlocks = function () {
    this.blocks.reportTensor = {
        only: SpriteMorph,
        type: 'reporter',
        category: 'tensorflow',
        spec: 'tensor from %l of type %tensortype',
        defaults: [null, 'float32']
    };

    this.blocks.reportRandomTensor = {
        only: SpriteMorph,
        type: 'reporter',
        category: 'tensorflow',
        spec: 'random tensor of shape %l from %tensorrandom',
        defaults: [null, 'randomNormal']
    };

    this.blocks.reportArithmeticOpers = {
        only: SpriteMorph,
        type: 'reporter',
        category: 'tensorflow',
        spec: 'arithmetic: %tensorarithmetic %l and %l of type %tensortype',
        defaults: ['add', null, null, 'float32']
    };

    this.blocks.reportBasicMathOpers = {
        only: SpriteMorph,
        type: 'reporter',
        category: 'tensorflow',
        spec: 'basic math: %tensorbasicmath of %l of type %tensortype',
        defaults: ['abs', null, 'float32']
    };

    this.blocks.reportMatricesOpers = {
        only: SpriteMorph,
        type: 'reporter',
        category: 'tensorflow',
        spec: 'matrices: %tensormatrices of %tensorlists of type %tensortype',
        defaults: ['dot', null, 'float32']
    };

    this.blocks.commandDispose = {
        only: SpriteMorph,
        type: 'command',
        category: 'tensorflow',
        spec: 'dispose %var',
    }

    this.blocks.reportInputLayer = {
        only: SpriteMorph,
        type: 'reporter',
        category: 'tensorflow',
        spec: 'input with shape %n',
        defaults: [10]
    };

    this.blocks.reportDenseLayer = {
        only: SpriteMorph,
        type: 'reporter',
        category: 'tensorflow',
        spec: 'dense with %n units and activation: %tensoractivations %br useBias: %bool type %tensortype',
        defaults: [10, 'relu', true, 'float32']
    };

    this.blocks.reportModel = {
        only: SpriteMorph,
        type: 'reporter',
        category: 'tensorflow',
        spec: 'model with input layer %s %br and %tensorlayers',
        //defaults: [10, 'relu', true, 'float32']
    };

    this.blocks.commandCompileModel = {
        only: SpriteMorph,
        type: 'command',
        category: 'tensorflow',
        spec: 'compile model %s with %br optimizer %tensoroptimizers learning rate %n loss %tensorloss and metric %tensormetric',
        defaults: [null, 'sgd', 0.001, 'meanSquaredError', 'accuracy']
    };

    this.blocks.commandModelFit = {
        only: SpriteMorph,
        type: 'command',
        category: 'tensorflow',
        //spec: 'fit model %s for data %l with labels %l %br %n epochs of batchSize %n %br loss %tensorloss acc %tensormetric',
        spec: 'fit model %s for data %l with labels %l %br %n epochs of batchSize %n',
        defaults: [null, null, null, 20, 10 /*"categoricalCrossentropy", "accuracy"*/]
    }

    this.blocks.reportPredict = {
        only: SpriteMorph,
        type: 'reporter',
        category: 'tensorflow',
        spec: 'predict %s using model %s',
        //defaults: [10, 'relu', true, 'float32']
    };
}

SpriteMorph.prototype.initBlocks = function () {
    this.originalInitBlocks();
    this.initTensorflowBlocks();
};

SpriteMorph.prototype.initBlocks();

// BlockTemplates decorator for Tensorflow
SpriteMorph.prototype.originalBlockTemplates = SpriteMorph.prototype.blockTemplates;
SpriteMorph.prototype.blockTemplates = function (category) {
    let blocks = this.originalBlockTemplates(category);

    function block(selector, isGhosted) {
        if (StageMorph.prototype.hiddenPrimitives[selector] && !all) {
            return null;
        }
        var newBlock = SpriteMorph.prototype.blockForSelector(selector, true);
        newBlock.isTemplate = true;
        if (isGhosted) { newBlock.ghost(); }
        return newBlock;
    }

    if (category === 'tensorflow') {
        blocks.push(block('reportTensor'));
        blocks.push(block('reportRandomTensor'));
        blocks.push(block('reportArithmeticOpers'));
        blocks.push(block('reportBasicMathOpers'));
        blocks.push(block('reportMatricesOpers'));
        blocks.push(block('commandDispose'));
        blocks.push(block('reportInputLayer'));
        blocks.push(block('reportDenseLayer'));
        blocks.push(block('reportModel'));
        blocks.push(block('commandCompileModel'));
        blocks.push(block('commandModelFit'));
        blocks.push(block('reportPredict'));
    }

    return blocks;
}

// This functions is overwritten because a problem ecountered with video and LML
StageMorph.prototype.drawOn = function (ctx, rect) {
    // draw pen trails and webcam layers
    var clipped = rect.intersect(this.bounds),
        pos, src, w, h, sl, st, ws, hs;

    if (!this.isVisible || !clipped.extent().gt(ZERO)) {
        return;
    }

    // costume, if any, and background color
    StageMorph.uber.drawOn.call(this, ctx, rect);

    pos = this.position();
    src = clipped.translateBy(pos.neg());
    sl = src.left();
    st = src.top();
    w = src.width();
    h = src.height();
    ws = w / this.scale;
    hs = h / this.scale;

    ctx.save();
    ctx.scale(this.scale, this.scale);

    // projection layer (e.g. webcam)
    /**
     * LML change
     * 
     * this lines have been added to solve a problem with video size.
     * I think is a Snap! bug and would be notified with a merge request
     */
    if (this.projectionSource) {
        if (this.projectionSource instanceof HTMLVideoElement) {
            w = this.projectionSource.videoWidth;
            h = this.projectionSource.videoHeight;
        }
        /**** end LML change problem with video size ****/

        ctx.globalAlpha = 1 - (this.projectionTransparency / 100);
        ctx.drawImage(
            this.projectionSource, // LML change -> this.proyectionLayeer() in the original
            sl / this.scale,
            st / this.scale,
            w, // LML change -> ws in the original
            h, // LML change -> hs in the original
            clipped.left() / this.scale,
            clipped.top() / this.scale,
            ws,
            hs
        );
        this.version = Date.now(); // update watcher icons
    }

    // pen trails
    ctx.globalAlpha = 1;
    ctx.drawImage(
        this.penTrails(),
        sl / this.scale,
        st / this.scale,
        ws,
        hs,
        clipped.left() / this.scale,
        clipped.top() / this.scale,
        ws,
        hs
    );

    ctx.restore();
};

// This functions is overwritten because a problem ecountered with video and LML
StageMorph.prototype.projectionSnap = function () {
    var snap = newCanvas(this.dimensions, true),
        ctx = snap.getContext('2d');
    /**
     * LML change
     * 
     * this set of number have been defined in an experimental way, taking into account
     * the modification made to drawOn method in order to get the whole camera image.
     * I don't know how to get them from the allowed methods of this object.
     * This mod is needed in order to get a complete image from my computer camera.
     * I have to try how the method works when other plugged cameras are used
     */
    ctx.drawImage(this.projectionSource, 0, 0, 640, 480, 0, 0, 480, 360); // LML change ->     ctx.drawImage(this.projectionLayer(), 0, 0); in the original
    /********* End LML change *********/

    return new Costume(snap, this.newCostumeName(localize('snap')));
};