function list2flatArray(list, arr) {
    let a = list.asArray();
    a.forEach(element => {
        if (typeof (element) != 'object') {
            arr.push(element);
            return arr;
        } else {
            return list2flatArray(element, arr);
        }
    });

    return arr;
}

Process.prototype.reportTensor = function (l, type = 'float32') {

    let a = (l instanceof List) ? list2flatArray(l, []) : l;
    let shape = (l instanceof List) ? l.shape().contents : [];
    let tensor;
    if (type != 'complex64') {
        tensor = tf.tensor(a, shape, type);
    } else {
        let real = a.map(x => x.split(",")[0]);
        let img = a.map(x => x.split(',')[1]);
        let t_real = tf.tensor(real, shape, 'float32');
        let t_img = tf.tensor(img, shape, 'float32');
        tensor = tf.complex(t_real, t_img);
    }
    console.log(tensor);
    return tensor;
}

Process.prototype.reportRandomTensor = function (l, random) {
    let t;
    let shape = l.asArray().map(x => parseInt(x));
    switch (random) {
        case 'randomNormal':
            t = tf.randomNormal(shape);
            break;
        case 'randomStandardNormal':
            t = tf.randomStandardNormal(shape);
            break;
        case 'randomUniform':
            t = tf.randomUniform(shape);
            break;
    }

    return t;
}

Process.prototype.reportArithmeticOpers = function (oper, t1, t2, type = 'float32') {

    t1 = (t1 instanceof tf.Tensor) ? t1 : this.reportTensor(t1, type);
    t2 = (t2 instanceof tf.Tensor) ? t2 : this.reportTensor(t2, type);
    let result;
    switch (oper) {
        case 'add':
            result = tf.add(t1, t2);
            break;
        case 'sub':
            result = tf.sub(t1, t2);
            break;
        case 'mul':
            result = tf.mul(t1, t2);
            break;
        case 'div':
            result = tf.div(t1, t2);
            break;
        case 'divNoNan':
            result = tf.divNoNan(t1, t2);
            break;
        case 'floorDiv':
            result = tf.floorDiv(t1, t2);
            break;
        case 'maximun':
            result = tf.maximum(t1, t2);
            break;
        case 'minimun':
            result = tf.minimum(t1, t2);
            break;
        case 'mod':
            result = tf.mod(t1, t2);
            break;
        case 'pow':
            result = tf.pow(t1, t2);
            break;
        case 'squaredDifference':
            result = tf.squaredDifference(t1, t2);
            break;
        default:
            console.log('Operación no válida');
            break;
    }
    tf.dispose(t1);
    tf.dispose(t2);
    console.log(tf.memory());

    return result;
}

Process.prototype.reportBasicMathOpers = function (oper, t, type) {
    t = (t instanceof tf.Tensor) ? t : this.reportTensor(t, type);
    let result;
    switch (oper) {
        case 'abs':
            result = tf.abs(t);
            break;
        case 'acos':
            result = tf.acos(t);
            break;
        case 'acosh':
            result = tf.acosh(t);
            break;
        case 'asin':
            result = tf.asin(t);
            break;
        case 'asinh':
            result = tf.asinh(t);
            break;
        case 'atan':
            result = tf.atan(t);
            break;
        case 'atan2':
            result = tf.atan2(t1, t2);
            break;
        case 'atanh':
            result = tf.atanh(t);
            break;
        case 'ceil':
            result = tf.ceil(t);
            break;
        case 'clipByValue':
            result = tf.clipByValue(t, min, max);
            break;
        case 'cos':
            result = tf.cos(t);
            break;
        case 'cosh':
            result = tf.cosh(t);
            break;
        case 'elu':
            result = tf.elu(t);
            break;
        case 'erf':
            result = tf.erf(t);
            break;
        case 'exp':
            result = tf.exp(t);
            break;
        case 'expm1':
            result = tf.expm1(t);
            break;
        case 'floor':
            result = tf.floor(t);
            break;
        case 'isFinite':
            result = tf.isFinite(t);
            break;
        case 'isInf':
            result = tf.isInf(t);
            break;
        case 'isNaN':
            result = tf.isNaN(t);
            break;
        case 'leakyRelu':
            result = tf.leakyRelu(t, alpha);
            break;
        case 'log':
            result = tf.log(t);
            break;
        case 'log1p':
            result = tf.log1p(t);
            break;
        case 'logSigmoid':
            result = tf.logSigmoid(t);
            break;
        case 'neg':
            result = tf.neg(t);
            break;
        case 'prelu':
            result = tf.prelu(t, alpha);
            break;
        case 'reciprocal':
            result = tf.reciprocal(t);
            break;
        case 'relu':
            result = tf.relu(t);
            break;
        case 'relu6':
            result = tf.relu6(t);
            break;
        case 'round':
            result = tf.round(t);
            break;
        case 'rsqrt':
            result = tf.rsqrt(t);
            break;
        case 'selu':
            result = tf.selu(t);
            break;
        case 'sigmoid':
            result = tf.sigmoid(t);
            break;
        case 'sign':
            result = tf.sign(t);
            break;
        case 'sin':
            result = tf.sin(t);
            break;
        case 'sinh':
            result = tf.sinh(t);
            break;
        case 'softplus':
            result = tf.softplus(t);
            break;
        case 'sqrt':
            result = tf.sqrt(t);
            break;
        case 'square':
            result = tf.square(t);
            break;
        case 'step':
            result = tf.step(t);
            break;
        case 'tan':
            result = tf.tan(t);
            break;
        case 'tanh':
            result = tf.tanh(t);
            break;
        default:
            console.log('Operación no válida');
            break;
    }
    tf.dispose(t);
    console.log(tf.memory());
    return result;
}

Process.prototype.reportMatricesOpers = function (oper, args, type) {
    let tensors = [];
    for (let l of args.asArray()) {
        let t = (l instanceof tf.Tensor) ? l : this.reportTensor(l, type);
        tensors.push(t);
    }
    let result;
    switch (oper) {
        case 'dot':
            if (tensors.length != 2) throw new Error("two tensors are needed to operate");
            result = tf.dot(tensors[0], tensors[1]);
            break;
        case 'euclideanNorm':
            if (tensors.length != 1) throw new Error("one only tensor is needed to operate");
            result = tf.euclideanNorm(tensors[0]);
            break;
        case 'norm':
            if (tensors.length != 1) throw new Error("one only tensor is needed to operate");
            result = tf.norm(tensors[0]);
            break;
        case 'outerProduct':
            if (tensors.length != 2) throw new Error("two tensors are needed to operate");
            result = tf.outerProduct(tensors[0], tensors[1]);
            break;
        case 'transpose':
            if (tensors.length != 1) throw new Error("one only tensor is needed to operate");
            result = tf.transpose(tensors[0]);
            break;
        default:
            console.log('Operación no válida');
            break;

    }

    for (let t of tensors) {
        tf.dispose(t);
    }

    console.log(tf.memory());

    return result;
}

Process.prototype.commandDispose = function (varName) {
    let t = this.variables.getVar(varName);
    tf.dispose(t);
    this.variables.setVar(varName, null);
    console.log(tf.memory());

}

Process.prototype.reportInputLayer = function (shape) {
    let input = tf.input({ shape: [shape] });
    console.log(input);
    return input;
}

Process.prototype.reportDenseLayer = function (units, activation, dtype = 'float32') {

    let args = {
        units: units,
        dtype: dtype
    }

    if (activation) args.activation = activation;

    let dense = tf.layers.dense(args);

    console.log(dense);
    return dense;
}

Process.prototype.reportModel = function (input, layers) {
    console.log(input);
    console.log(layers);
    let output;
    for (let i = 0; i < layers.contents.length; i++) {
        if (i == 0) {
            output = layers.contents[i].apply(input);
        } else {
            output = layers.contents[i].apply(output)
        }
    }

    let model = tf.model({ inputs: input, outputs: output });
    console.log(model.summary());
    return model;
}

Process.prototype.reportPredict = function (t, model) {
    let p = model.predict(t);
    console.log(p);
    return p;
}

Process.prototype.commandCompileModel = function (model, optimizer, learningrate, loss, metric) {
    let opt;
    switch (optimizer) {
        case 'sgd':
            opt = tf.train.sgd(learningrate);
            break;
        case 'momentum':
            opt = tf.train.momentum(learningrate);
            break;
        case 'adagrad':
            opt = tf.train.adagrad(learningrate);
            break;
        case 'adadelta':
            opt = tf.train.adadelta(learningrate);
            break;
        case 'adam':
            opt = tf.train.adam(learningrate);
            break;
        case 'adamax':
            opt = tf.train.adamax(learningrate);
            break;
        case 'rmsprop':
            opt = tf.train.rmsprop(learningrate);
            break;
    }
    
    model.compile({
        optimizer: opt,
        loss: loss,
        metrics: [metric]
    });

    console.log(model);
}

Process.prototype.commandModelFit = function (model, data, labels, epochs, batchSize /*loss, accuracy*/) {

    let self = this;
    let onBatchEnd = (batch, logs) => {
        //self.variables.setVar(loss, logs.loss);
        //self.variables.setVar(accuracy, logs.acc)
        console.log(`accuracy ${logs.acc}, loss: ${logs.loss}`);
    }

    /**
     * first, an accumulator attribute associated to the object `this.context` is created
     * if it doesn't exist. 
     */
    if (isNil(this.context.accumulator)) {
        this.context.accumulator = {
            ready: false,
            started: false,
            history: null
        };
    }

    let acc = this.context.accumulator;

    if (acc.ready) {
        return acc.history;
    }
    console.log("AQUIIIII");
    if (!acc.started) {
        model.fit(data, labels, {
            epochs: epochs,
            batchSize: batchSize,
            callbacks: { onBatchEnd }
        }).then(info => {
            console.log('Final accuracy', info.history.acc);
            console.log(tf.memory());
            acc.ready = true;
            acc.history = info.history;
        });

        acc.started = true;
    }

    this.pushContext('doYield');
    this.pushContext();
}